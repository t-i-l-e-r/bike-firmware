
/* ###################################################################
**     Filename    : main.c
**     Project     : lpuart_echo_s32k118
**     Processor   : S32K118_64
**     Version     : Driver 01.00
**     Compiler    : GNU C Compiler
**     Date/Time   : 2017-08-04, 12:27, # CodeGen: 1
**     Abstract    :
**         Main module.
**         This module contains user's application code.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/*!
** @file main.c
** @version 01.00
** @brief
**         Main module.
**         This module contains user's application code.
*/
/*!
**  @addtogroup main_module main module documentation
**  @{
*/
/* MODULE main */

/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "pin_mux.h"
#include "dmaController1.h"
#include "clockMan1.h"
#include "lpuart1.h"
volatile int exit_code = 0;
/* User includes (#include below this line is not maintained by Processor Expert) */
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#define NotCharging "NoCharge\r\n"  //send OFF to disable the IR211 driver
#define BatteryFault  "BattFault\r\n"  //send ON to enable the IR211 driver
#define Charging "Charging\r\n"  //send OFF to disable the IR211 driver
#define NTCFault  "NTCFault\r\n"  //send ON to enable the IR211 driver
/* Welcome message displayed at the console */
#define Welcome "Welcome\r\n"


//delay function
    void delay(volatile uint32_t cycles)
    {
        while (cycles--)
        {
        }
    }


//GPIO interrupt
/*void PORTA_IRQH(void)
{
	 PINS_DRV_ClearPortIntFlagCmd(PORTA);
	 PINS_DRV_TogglePins(PTD, (1 << 6U)); //red LED 0n
	 LPUART_DRV_SendData (INST_LPUART1, (uint8_t *)Shutdown, strlen(Shutdown));

}*/

int main(void)

{
  /* Write your local variable definition here */

  /* Declare a buffer used to store the received data */
  ftm_state_t ftmStateStruct;
  uint64_t dutyCycle = 0x0000; //0x8000=100%, 0x4000=50%...8000Hz period
 int status=0;




  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
#ifdef PEX_RTOS_INIT
  PEX_RTOS_INIT(); /* Initialization of the selected RTOS. Macro is defined by the RTOS component. */

#endif
  /*** End of Processor Expert internal initialization.                    ***/

  /* Initialize and configure clocks
   *     -    see clock manager component for details
   */
  CLOCK_SYS_Init(g_clockManConfigsArr, CLOCK_MANAGER_CONFIG_CNT,
                 g_clockManCallbacksArr, CLOCK_MANAGER_CALLBACK_CNT);  CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_AGREEMENT);

  /* Initialize pins
   *    -    See PinSettings component for more info
   */
  PINS_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);
  PINS_DRV_SetPins(PTD, (1 << 6U)|(1 << 7U));

  /* Initialize LPUART instance */
  LPUART_DRV_Init(INST_LPUART1, &lpuart1_State, &lpuart1_InitConfig0);
 // LPUART_DRV_SendData (INST_LPUART1, (uint8_t *)Welcome, strlen(Welcome));



 /* Initialize FTM PWM channel 0 PTB3 period 8kHz, see config*/
  FTM_DRV_Init(INST_FLEXTIMER_PWM1, &flexTimer_pwm1_InitConfig, &ftmStateStruct);
  FTM_DRV_InitPwm(INST_FLEXTIMER_PWM1, &flexTimer_pwm1_PwmConfig);
  //PWM load control
  FTM_DRV_UpdatePwmChannel(INST_FLEXTIMER_PWM1, 1U, FTM_PWM_UPDATE_IN_DUTY_CYCLE, dutyCycle, 0U, true);


 	  /* Initialize GPIO interrupt rise edge PTA12 -STAT1*/
 	 //INT_SYS_InstallHandler(PORT_IRQn,&PORTA_IRQH, (isr_t *)0 );
 	// INT_SYS_EnableIRQ(PORT_IRQn);

  while (1)

  {
	  PINS_DRV_TogglePins(PTD, (1 << 7U)); //green led
	  delay(10000000);
	  status=PINS_DRV_ReadPins(PTA);
	  status&=0x3000;  //mask other inputs state and see the status of PTA12 and PTA13, 0x3000=2^12+2^13 =pin 12&pin 13

	  if(status==0x1000) //if PTA12-high (Stat1) and PTA13-low (Stat2)
	  {
		  PINS_DRV_SetPins(PTD, (1 << 15U)); // yellow off
		  PINS_DRV_ClearPins(PTD, (1 << 16U)); //blue on
	      LPUART_DRV_SendData (INST_LPUART1, (uint8_t *)BatteryFault, strlen(BatteryFault));
	  }

	  if(status==0x2000) //if PTA12-low, PTA13-high
	  {
		  PINS_DRV_ClearPins(PTD, (1 << 15U)); //yellow on
		  PINS_DRV_SetPins(PTD, (1 << 16U)); //blue off
		  //LPUART_DRV_SendData (INST_LPUART1, (uint8_t *)Charging, strlen(Charging));
	  }

	  if(status==0x3000) //if PTA12-high, PTA13-high
	  {
		  PINS_DRV_SetPins(PTD, (1 << 15U)); //yellow off
		  PINS_DRV_SetPins(PTD, (1 << 16U)); //blue off
		  LPUART_DRV_SendData (INST_LPUART1, (uint8_t *)NotCharging, strlen(NotCharging));
	  }

	  if(status==0x0000) //if PTA12-low, PTA13-low
		{
		  PINS_DRV_ClearPins(PTD, (1 << 15U)); //yellow on
		  PINS_DRV_ClearPins(PTD, (1 << 16U)); //blue on
	      LPUART_DRV_SendData (INST_LPUART1, (uint8_t *)NTCFault , strlen(NTCFault ));
	    }
  }
  /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** RTOS startup code. Macro PEX_RTOS_START is defined by the RTOS component. DON'T MODIFY THIS CODE!!! ***/
  #ifdef PEX_RTOS_START
    PEX_RTOS_START();                  /* Startup of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of RTOS startup code.  ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;) {
    if(exit_code != 0) {
      break;
    }
  }
  return exit_code;
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/* END main */
/*!
 ** @}
 */
/*
 ** ###################################################################
 **
 **     This file was created by Processor Expert 10.1 [05.21]
 **     for the Freescale S32K series of microcontrollers.
 **
 ** ###################################################################
 */
